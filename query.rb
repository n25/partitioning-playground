require 'optparse'
require 'benchmark'

require_relative 'setup'
require_relative 'index'

$options = {}
OptionParser.new do |opts|
    opts.banner = "Usage: query.rb [options]"
    opts.on("-a", "--analyze", "Run analysis") { |a| $options[:analyze] = true }
    opts.on_tail("-h", "--help", "Show this message") { puts opts ; exit }
end.parse!

module Query
  def self.benchmark(&block)
    Benchmark.bm do |bm|
      bm.report { block.call }
    end
  end

  def self.run(query)
    puts query.sql

    if $options[:analyze]
      puts query.analyze
    else
      benchmark { DB << query.sql }
    end

    puts ''
  end

  autoload :Naive,        './query/naive'
  autoload :OnlyThisYear, './query/only_this_year'
  autoload :Direct,       './query/direct'
end

{
  'no index' => [],

  'fields index' => [
    proc do |table_name, index_count|
      %[CREATE INDEX "#{table_name}_index_#{index_count}" ON "public"."#{table_name}" (hidden, archived_at, deleted_at)]
    end,
  ],

  'fields & channel index' => [
    proc do |table_name, index_count|
      %[CREATE INDEX "#{table_name}_index_#{index_count}" ON "public"."#{table_name}" (hidden, archived_at, deleted_at)]
    end,

    proc do |table_name, index_count|
      %[CREATE INDEX "#{table_name}_index_#{index_count}" ON "public"."#{table_name}" (channel_id)]
    end,
  ],


  'fields & sort key index' => [
    proc do |table_name, index_count|
      %[CREATE INDEX "#{table_name}_index_#{index_count}" ON "public"."#{table_name}" (hidden, archived_at, deleted_at)]
    end,

    proc do |table_name, index_count|
      %[CREATE INDEX "#{table_name}_index_#{index_count}" ON "public"."#{table_name}" (channel_id)]
    end,

    proc do |table_name, index_count|
      %[CREATE INDEX "#{table_name}_index_#{index_count}" ON "public"."#{table_name}" (remote_created_at)]
    end,
  ]
}.each do |name, index_list|
  puts "================================================"
  puts "================ #{name.upcase} ================"
  puts "================================================"

  indexer = Index::Base.new('messages')
  indexer.drop_all_index
  index_list.each { |block| indexer.create_index(&block) }

  Query.run Query::Naive.new.query
  Query.run Query::OnlyThisYear.new.query(2014)
  Query.run Query::Direct.new.query(2014, 1)
end


