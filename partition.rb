require_relative 'setup'

module Partition
  autoload :ByTimeOnly,       './partition/by_time_only'
  autoload :ByTimeAndChannel, './partition/by_time_and_channel'
  autoload :ByTimeAndAccount, './partition/by_time_and_account'
end
