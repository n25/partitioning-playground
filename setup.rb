# Setup the database.
#
# This require a local access to the database through the
# 'postgres' user and at le 'localhost' host.

require 'pg'
require 'sequel'

def create_and_connect_to(database_url, database_name)
  administrative_database = Sequel.connect("postgres://#{database_url}/postgres")
  if administrative_database[:pg_database].where(datname: database_name).first
    puts "Database #{database_name} already exist."
  else
    puts "Create database #{database_name}..."
    administrative_database << "CREATE DATABASE #{database_name}"
  end
  administrative_database.disconnect

  Sequel.connect("postgres://#{database_url}/#{database_name}")
end

database_name = 'partitioning_pg'
database_url = "postgres@localhost:5433"

DB = create_and_connect_to(database_url, database_name)
