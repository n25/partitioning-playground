require_relative 'setup'
require_relative 'partition'

#
# Create the master table.
#

DB.drop_table?(:messages, cascade: true)
DB.create_table(:messages) do
  primary_key :id
  Integer     :account_id
  Integer     :channel_id
  TrueClass   :hidden
  Time        :archived_at
  Time        :deleted_at
  Time        :remote_created_at
end

#
# Create the partitions
#

# TODO: Allow the partition stategy to be changed
# Partition::ByTimeAndAccount.add_partioning_tiggers(:messages, time: :remote_created_at, account: :account_id)
Partition::ByTimeAndChannel.add_partioning_tiggers(:messages, time: :remote_created_at, channel: :channel_id)

#
# Insert fake data over time
#

# TODO: Migrate the following as script parameters
hidden_percent     = 20
archived_percent   = 90
deleted_percent    = 10
number_of_accounts = 10
number_of_channels = 10
messages_per_month = 50_000

(0..1).each do |year_diff|
  (0..11).each do |month_diff|
    puts "Inserting #{messages_per_month} messages arround #{year_diff} year(s) and #{month_diff} month(s) from now..."
    DB << <<-SQL
      insert into messages (account_id, channel_id, hidden, archived_at, deleted_at, remote_created_at)
      select
        (t.count % #{number_of_accounts} + 1)                                               as account_id,
        (t.count % #{number_of_channels} + 1)                                               as channel_id,
        (ROUND(random() * 100) <= #{hidden_percent})                                        as hidden,
        (CASE WHEN (ROUND(random() * 100) <= #{archived_percent}) THEN now() ELSE NULL END) as archived_at,
        (CASE WHEN (ROUND(random() * 100) <= #{deleted_percent}) THEN now() ELSE NULL END)  as deleted_at,
        (now() - '#{year_diff} year'::INTERVAL
               - '#{month_diff} month'::INTERVAL
               - '1 day'::INTERVAL * ROUND(random() * 31)
               - '1 hour'::INTERVAL * ROUND(random() * 24)
               - '1 minute'::INTERVAL * ROUND(random() * 60)
        )                                                                                   as remote_created_at
      from (select * from generate_series(1,#{messages_per_month}) as count) as t;
    SQL
  end
end
