module Query
  class Naive
    def query
      DB[:messages]
        .where(
          account_id: 1,
          channel_id: [1, 2, 3, 4, 5],
          hidden: false,
          archived_at: nil,
          deleted_at: nil
        )
        .order{remote_created_at.desc}
        .limit(30, 0)
    end
  end
end
