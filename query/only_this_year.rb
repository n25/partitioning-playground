module Query
  class OnlyThisYear < Naive
    def query(year)
      this_year ||= Time.new(year, 1, 1, 0, 0, 0)
      next_year ||= Time.new(year + 1, 1, 1, 0, 0, 0)
      super().where(remote_created_at: this_year...next_year)
    end
  end
end
