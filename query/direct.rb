module Query
  class Direct
    def query(year, account_id)
      DB[:"messages_#{year}_#{account_id}"]
        .where(
          channel_id: [1, 2, 3, 4, 5],
          hidden: false,
          archived_at: nil,
          deleted_at: nil
        )
        .order{remote_created_at.desc}
        .limit(30, 0)
    end
  end
end
