module Partition
  class ByTimeAndAccount
    def self.add_partioning_tiggers(master_table, time: , account: , timeframe: :year)
      DB.create_function(
        "#{master_table}_partition_fn",
        function_definition(master_table, time, account, timeframe),
        language: 'plpgsql',
        returns: 'TRIGGER',
        replace: true,
      )

      DB.create_trigger(
        master_table,
        "#{master_table}_trigger",
        "#{master_table}_partition_fn",
        events: [:insert],
        each_row: true
      )
    end

    def self.function_definition(master_table, time, account, timeframe)
      case timeframe
      when :year
        start_date_expr       = "date_trunc('year', NEW.\"#{time}\")"
        end_date_expr         = "_startdate + INTERVAL '1 year'"
        partition_suffix_expr = "to_char(_startdate,'yyyy')||'_'||NEW.\"#{account}\""
      else
        raise "unsupported timeframe: #{timeframe}"
      end

      definition = <<-SQL
        DECLARE
          _tablename text;
          _startdate timestamp;
          _enddate timestamp;
          _result record;
        BEGIN
          IF NEW."#{time}" IS NULL THEN
            RAISE EXCEPTION 'must supply a #{time}';
          END IF;

          --Takes the current inbound "#{time}" value and determines when midnight is for the given date
          _startdate := #{start_date_expr};
          _tablename := '#{master_table}_'||#{partition_suffix_expr};

          -- Check if the partition needed for the current record exists
          PERFORM 1
          FROM   pg_catalog.pg_class c
          JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
          WHERE  c.relkind = 'r'
          AND    c.relname = _tablename
          AND    n.nspname = 'public';

          -- If the partition needed does not yet exist, then we create it:
          IF NOT FOUND THEN
            _enddate := #{end_date_expr};
            EXECUTE 'CREATE TABLE public.' || quote_ident(_tablename) || ' (
              CHECK ( "#{time}" >= ' || quote_literal(_startdate) || ' AND "#{time}" < '|| quote_literal(_enddate) || '),
              CHECK ( "#{account}" = ' || quote_literal(NEW.#{account}) || ')
            ) INHERITS ( public.#{master_table} )';

            -- EXECUTE 'CREATE INDEX ' || quote_ident(_tablename||'_index1') || '
            -- ON public.' || quote_ident(_tablename) || ' (channel_id)';

            -- EXECUTE 'CREATE INDEX ' || quote_ident(_tablename||'_index2') || '
            -- ON public.' || quote_ident(_tablename) || ' (hidden, archived_at, deleted_at, channel_id)';

            -- EXECUTE 'CREATE INDEX ' || quote_ident(_tablename||'_index3') || '
            -- ON public.' || quote_ident(_tablename) || ' (remote_created_at)';
          END IF;

          -- Insert the current record into the correct partition, which we are sure will now exist.
          EXECUTE 'INSERT INTO public.' || quote_ident(_tablename) || ' VALUES ($1.*)' USING NEW;
          RETURN NULL;
        END
      SQL
    end
  end
end

