module Index
  class Base < Struct.new(:table_name)
    def create_index(&block)
      partitions = fetch_partitions
      indexes = fetch_indexes

      partitions.each do |table_name|
        index_count = indexes[table_name].count
        query = block.call(table_name, index_count)
        DB << query;
      end
    end

    def drop_all_index
      indexes = fetch_indexes
      partitions = fetch_partitions

      partitions.each do |table_name|
        indexes[table_name].each do |index_name|
          query = %[DROP INDEX IF EXISTS "#{index_name}"]
          DB << query;
        end
      end
    end

    def fetch_partitions
      query = <<-SQL
        SELECT c.relname
        FROM   pg_catalog.pg_class c
        JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE  c.relkind = 'r'
        AND    c.relname LIKE '#{table_name}_%'
        AND    n.nspname = 'public';
      SQL
      result = DB.fetch(query).all;
      result.to_a.map { |row| row[:relname] }
    end

    def fetch_indexes
      query = <<-SQL
        SELECT
          c.relname as "name",
          c2.relname as "table"
        FROM pg_catalog.pg_class c
        JOIN pg_catalog.pg_index i
          ON i.indexrelid = c.oid
        JOIN pg_catalog.pg_class c2
          ON i.indrelid = c2.oid
        LEFT JOIN pg_catalog.pg_namespace n
          ON n.oid = c.relnamespace
        WHERE
              c.relkind = 'i'
          AND c2.relname LIKE '#{table_name}_%'
          AND n.nspname NOT IN ('pg_catalog', 'pg_toast')
          AND pg_catalog.pg_table_is_visible(c.oid);
      SQL
      result = DB.fetch(query).all;
      hash = Hash.new { |this, key| this[key] = [] }
      result.to_a.each_with_object(hash) do |row, hash|
        table_name = row[:table]
        index_name = row[:name]
        hash[table_name] << index_name
      end
    end

  end
end
